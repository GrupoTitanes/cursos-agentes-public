namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AgentesUsuarios
    {
        [Key]
        public int id_usuarios { get; set; }

        [StringLength(1)]
        public string TipoUsuario { get; set; }

        [Required]
        [StringLength(10)]
        public string Agente { get; set; }

        [StringLength(12)]
        public string Login { get; set; }

        [StringLength(20)]
        public string Password { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(100)]
        public string Apellidos { get; set; }

        [StringLength(20)]
        public string Documento { get; set; }

        [StringLength(1)]
        public string Validado { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? FechaCaducidad { get; set; }

        public int? moodle { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Alta { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? Baja { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CursoRealizado { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? TopeCurso { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UltimoAcceso { get; set; }

        [StringLength(10)]
        public string CodigoGrupo { get; set; }

        public decimal? Nota { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Curso { get; set; }

        [StringLength(8)]
        public string ClaveGiro { get; set; }

        [StringLength(10)]
        public string Avisos { get; set; }

        [StringLength(15)]
        public string Movil { get; set; }

        public byte? MovilEstado { get; set; }

        public byte? AnexoEstado { get; set; }

        public byte? MovilAvisos { get; set; }

        public int? UsuarioValidacion { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CaducidadDIN { get; set; }
    }
}
