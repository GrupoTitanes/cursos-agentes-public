namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ComercialesAgentes
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id_agente { get; set; }

        [StringLength(10)]
        public string Codigo { get; set; }

        [StringLength(50)]
        public string RazonSocial { get; set; }

        [StringLength(50)]
        public string NombreComercial { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(50)]
        public string Apellido1 { get; set; }

        [StringLength(50)]
        public string Apellido2 { get; set; }

        [StringLength(50)]
        public string Direccion { get; set; }

        [StringLength(10)]
        public string CP { get; set; }

        [StringLength(30)]
        public string Poblacion { get; set; }

        [StringLength(10)]
        public string Nacion { get; set; }

        [StringLength(20)]
        public string DIN { get; set; }

        [StringLength(5)]
        public string TipoMov { get; set; }

        [StringLength(15)]
        public string Telefono { get; set; }

        [StringLength(15)]
        public string Fax { get; set; }

        [StringLength(30)]
        public string CC { get; set; }

        [StringLength(255)]
        public string Observaciones { get; set; }

        [StringLength(1)]
        public string TipoCodigo { get; set; }

        [StringLength(20)]
        public string FechaNacimiento { get; set; }

        [StringLength(2)]
        public string TipoVia { get; set; }

        [StringLength(15)]
        public string NumVia { get; set; }

        [StringLength(50)]
        public string ApellidoSocio2 { get; set; }

        [StringLength(50)]
        public string ApellidoSocio1 { get; set; }

        [StringLength(20)]
        public string NombreSocio1 { get; set; }

        [StringLength(20)]
        public string NombreSocio2 { get; set; }

        [StringLength(50)]
        public string CodigoSocio1 { get; set; }

        [StringLength(50)]
        public string CodigoSocio2 { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? FechaContrato { get; set; }

        [StringLength(15)]
        public string movil { get; set; }

        public int? comercial { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(1)]
        public string viajes { get; set; }

        [StringLength(2)]
        public string TipoEmpresa { get; set; }

        [StringLength(1)]
        public string FichaCompleta { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(10)]
        public string codigo2 { get; set; }

        [StringLength(10)]
        public string Origen { get; set; }

        public short? NumVisitas { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? FechaVisita { get; set; }

        public short? Envios { get; set; }

        public int? Volumen { get; set; }

        [StringLength(255)]
        public string Paises { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? UltimaVisita { get; set; }

        public short? promedio { get; set; }

        public short? acumulado { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? fechacaducidad { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ContratoAlquiler { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CursoRealizado { get; set; }

        public decimal? Nota { get; set; }
    }
}
