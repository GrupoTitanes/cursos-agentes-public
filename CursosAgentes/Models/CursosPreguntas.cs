namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosPreguntas
    {
        public int ID { get; set; }

        public string TEXTO { get; set; }

        public int? ID_TEXT { get; set; }
    }
}
