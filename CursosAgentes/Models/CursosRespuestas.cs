namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosRespuestas
    {
        public int ID { get; set; }

        public string TEXTO { get; set; }

        public int? ID_PREGUNTA { get; set; }

        public int? CORRECTA { get; set; }
    }
}
