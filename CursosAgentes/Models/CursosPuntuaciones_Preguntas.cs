namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosPuntuaciones_Preguntas
    {
        public int ID { get; set; }

        public int? ID_PREG { get; set; }

        public DateTime? FECHA_CREACION { get; set; }

        public int? VALOR { get; set; }

        public int? ID_RESPUESTAS { get; set; }

        public int? ID_TEXTS { get; set; }

        public int? ID_USUARIOS { get; set; }

        public int? ID_PROSPECTO { get; set; }
    }
}
