namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosUsuarios
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string USUARIO { get; set; }

        [StringLength(50)]
        public string PASSWORD { get; set; }

        [StringLength(50)]
        public string DNI { get; set; }

        public int? CURSO { get; set; }

        public DateTime? FECHA_CREACION { get; set; }

        public int? ADMIN { get; set; }

        public int? COMERCIAL { get; set; }

        public int? AGENTE { get; set; }
    }
}
