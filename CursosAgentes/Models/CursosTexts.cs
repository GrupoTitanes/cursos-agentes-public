namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosTexts
    {
        public int ID { get; set; }

        public string NOMBRE { get; set; }

        public int? ID_CURSO { get; set; }

        public DateTime? FECHA_CREACION { get; set; }

        public int? POSICION { get; set; }
    }
}
