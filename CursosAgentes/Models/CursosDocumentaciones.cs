namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosDocumentaciones
    {
        public int ID { get; set; }

        public string NOMBRE { get; set; }

        public int? ID_CURSO { get; set; }

        public string URL { get; set; }
    }
}
