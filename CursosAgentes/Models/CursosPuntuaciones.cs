namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosPuntuaciones
    {
        public int ID { get; set; }

        public int? RESULTADO { get; set; }

        public int? ESTADO { get; set; }

        public int? ID_TEXTS { get; set; }

        public int? ID_USUARIOS { get; set; }

        public int? ID_PROSPECTO { get; set; }
    }
}
