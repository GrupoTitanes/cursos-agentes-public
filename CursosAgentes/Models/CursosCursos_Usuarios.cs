namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosCursos_Usuarios
    {
        public int ID { get; set; }

        public int? ESTADO { get; set; }

        public DateTime? FECHA_REALIZADO { get; set; }

        public int? ID_CURSOS { get; set; }

        public int? ID_USUARIOS { get; set; }

        public decimal? RESULTADO { get; set; }

        public int? ID_PROSPECTO { get; set; }
    }
}
