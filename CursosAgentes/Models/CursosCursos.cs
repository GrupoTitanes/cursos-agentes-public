namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CursosCursos
    {
        public int ID { get; set; }

        public string NOMBRE { get; set; }

        public DateTime? FECHA_CREACION { get; set; }

        public int? USU_CREACION { get; set; }
    }
}
