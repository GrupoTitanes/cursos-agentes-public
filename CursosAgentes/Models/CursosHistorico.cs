namespace CursosAgentes.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CursosHistorico")]
    public partial class CursosHistorico
    {
        public int Id { get; set; }

        public int IdCurso { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Fecha { get; set; }

        public decimal Nota { get; set; }

        public int? IdAgente { get; set; }

        public int? IdProspecto { get; set; }
    }
}
