﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CursosAgentes
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{pais}",
                defaults: new { controller = "Home", action = "Index", pais = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "AdminLogin",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Admin", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Login",
                url: "{controller}/{action}/",
                defaults: new { controller = "Cursos", action = "Index" }
            );
            routes.MapRoute(
                name: "LoginProspecto",
                url: "{controller}/{action}/",
                defaults: new { controller = "Cursos", action = "IndexProspecto" }
            );
            routes.MapRoute(
                name: "UserLogin",
                url: "{controller}/{action}/{dni}/{usuario}/{password}",
                defaults: new { controller = "Cursos", action = "Index", dni = UrlParameter.Optional, usuario = UrlParameter.Optional, password = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "accederCurso",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Cursos", action = "accederCurso", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "accederText",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Cursos", action = "accederText", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "verCertificado",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Cursos", action = "verCertificado", id = UrlParameter.Optional }
            );
        }
    }
}
