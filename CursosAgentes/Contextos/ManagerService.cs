﻿using CursosAgentes.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CursosAgentes.Contextos;
using System.Globalization;
using System.Threading;

namespace CursosAgentes.Contextos
{
    public class ManagerService
    {
        private const string providerName = "System.Data.SqlClient";
        private const string ESConnectionString = "GrupoTitanesES";
        private const string ITConnectionString = "GrupoTitanesIT";

        public GrupoTitanes contextES;
        public GrupoTitanes contextIT;

        public ManagerService()
        {
            var ConfigConectionString = ConfigurationManager.ConnectionStrings[ESConnectionString];
            var ITConfigConectionString = ConfigurationManager.ConnectionStrings[ITConnectionString];

            contextES = new GrupoTitanes(CreateDbConnection(ConfigConectionString.ConnectionString));
            contextIT = new GrupoTitanes(CreateDbConnection(ITConfigConectionString.ConnectionString));
        }

        public ComercialesAgentes FindByDniPhonePasswordProspecto(string dni, string telefono, string password,out string context) {

            ComercialesAgentes usu_com = null;
            usu_com=contextES.ComercialesAgentes.Where(x => x.DIN.ToUpper() == dni && x.Telefono.ToUpper() == telefono && x.DIN.ToUpper() == password).FirstOrDefault();

            if (usu_com == null)
            {

                usu_com = contextIT.ComercialesAgentes.Where(x => x.DIN.ToUpper() == dni && x.Telefono.ToUpper() == telefono && x.DIN.ToUpper() == password).FirstOrDefault();

                context = usu_com != null ? "IT" : string.Empty;
            }
            else
                context = "ES";

            return usu_com;
        }

        public AgentesUsuarios FindByDniPhonePasswordAgente(string dni, string usuario, string password, out string context)
        {

            AgentesUsuarios ag = null;
            ag = contextES.AgentesUsuarios.Where(x => x.Documento == dni && x.Login == usuario && x.Documento == password && x.Validado != "N").FirstOrDefault();

            if (ag == null)
            {
                ag = contextIT.AgentesUsuarios.Where(x => x.Documento == dni && x.Login == usuario && x.Documento == password && x.Validado != "N").FirstOrDefault();
                context = ag == null ? string.Empty : "IT";
            }
            else
                context = "ES";

            return ag;
        }

        public AgentesUsuarios FindByDniPhonePasswordAgenteDirect(string dni, string usuario, string password, string context)
        {

            AgentesUsuarios ag = null;
            ag = GetContext(context).AgentesUsuarios.Where(x => x.Documento == dni && x.Login == usuario && x.Documento == password && x.Validado != "N").FirstOrDefault();

            if (ag == null) {
                context = "IT";
                ag = GetContext(context).AgentesUsuarios.Where(x => x.Documento == dni && x.Login == usuario && x.Documento == password && x.Validado != "N").FirstOrDefault();
            }

            return ag;
        }

        public AgentesUsuarios FindByDniPhonePasswordObject(ComercialesAgentes usu_com)
        {

            AgentesUsuarios ag = null;

            if (usu_com != null) {
                ag = contextES.AgentesUsuarios.Where(x => x.Documento.ToUpper() == usu_com.DIN.ToUpper() && x.Agente == usu_com.Codigo && x.Validado != "N").FirstOrDefault();

                if (ag == null)
                {
                    ag = contextIT.AgentesUsuarios.Where(x => x.Documento.ToUpper() == usu_com.DIN.ToUpper() && x.Agente == usu_com.Codigo && x.Validado != "N").FirstOrDefault();
                }

            }

            return ag;
        }

        public GrupoTitanes GetContext(string contexto) {

            if (contexto != "ES")
            {

                var language = new CultureInfo("it-IT");
                Thread.CurrentThread.CurrentUICulture = language;
            }

            return !string.IsNullOrEmpty(contexto) ?
                contexto == "ES" ? this.contextES : this.contextIT 
                : null;
        }

        private DbConnection CreateDbConnection(string connectionString)
        {
            // Assume failure.
            DbConnection connection = null;

            // Create the DbProviderFactory and DbConnection. 
            if (connectionString != null)
            {
                DbProviderFactory factory = DbProviderFactories.GetFactory(providerName);
                connection = factory.CreateConnection();
                if (connection != null) connection.ConnectionString = connectionString;
            }
            return connection;
        }
    }
}