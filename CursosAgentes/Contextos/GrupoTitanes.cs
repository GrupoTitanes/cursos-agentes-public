namespace CursosAgentes.Contextos
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;
    using CursosAgentes.Models;
    using System.Data.Common;

    public partial class GrupoTitanes : DbContext
    {


        public GrupoTitanes(DbConnection dbConnection) : base(dbConnection, true)
        {

        }


        public void subirDocumentos(string ruta, HttpPostedFileBase file)
        {

            file.SaveAs("//192.168.10.222/Cursos/Temario/" + file.FileName);
        }

        public virtual DbSet<AgentesUsuarios> AgentesUsuarios { get; set; }
        public virtual DbSet<CursosCursos> CursosCursos { get; set; }
        public virtual DbSet<CursosCursos_Usuarios> CursosCursos_Usuarios { get; set; }
        public virtual DbSet<CursosDocumentaciones> CursosDocumentaciones { get; set; }
        public virtual DbSet<CursosPreguntas> CursosPreguntas { get; set; }
        public virtual DbSet<CursosPuntuaciones> CursosPuntuaciones { get; set; }
        public virtual DbSet<CursosPuntuaciones_Preguntas> CursosPuntuaciones_Preguntas { get; set; }
        public virtual DbSet<CursosRespuestas> CursosRespuestas { get; set; }
        public virtual DbSet<CursosTexts> CursosTexts { get; set; }
        public virtual DbSet<CursosUsuarios> CursosUsuarios { get; set; }
        public virtual DbSet<ComercialesAgentes> ComercialesAgentes { get; set; }
        public virtual DbSet<CursosHistorico> CursosHistorico { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.TipoUsuario)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Agente)
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Login)
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Apellidos)
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Documento)
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Validado)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Nota)
                .HasPrecision(18, 5);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Curso)
                .HasPrecision(18, 0);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.ClaveGiro)
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Avisos)
                .IsUnicode(false);

            modelBuilder.Entity<AgentesUsuarios>()
                .Property(e => e.Movil)
                .IsUnicode(false);

            modelBuilder.Entity<CursosCursos>()
                .Property(e => e.NOMBRE)
                .IsUnicode(false);

            modelBuilder.Entity<CursosCursos_Usuarios>()
                .Property(e => e.RESULTADO)
                .HasPrecision(18, 5);

            modelBuilder.Entity<CursosDocumentaciones>()
                .Property(e => e.NOMBRE)
                .IsUnicode(false);

            modelBuilder.Entity<CursosDocumentaciones>()
                .Property(e => e.URL)
                .IsUnicode(false);

            modelBuilder.Entity<CursosPreguntas>()
                .Property(e => e.TEXTO)
                .IsUnicode(false);

            modelBuilder.Entity<CursosRespuestas>()
                .Property(e => e.TEXTO)
                .IsUnicode(false);

            modelBuilder.Entity<CursosTexts>()
                .Property(e => e.NOMBRE)
                .IsUnicode(false);

            modelBuilder.Entity<CursosUsuarios>()
                .Property(e => e.USUARIO)
                .IsUnicode(false);

            modelBuilder.Entity<CursosUsuarios>()
                .Property(e => e.PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<CursosUsuarios>()
                .Property(e => e.DNI)
                .IsUnicode(false);

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.ApellidoSocio2)
                .IsFixedLength();

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.ApellidoSocio1)
                .IsFixedLength();

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.NombreSocio1)
                .IsFixedLength();

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.NombreSocio2)
                .IsFixedLength();

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.CodigoSocio1)
                .IsFixedLength();

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.CodigoSocio2)
                .IsFixedLength();

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.viajes)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.TipoEmpresa)
                .IsUnicode(false);

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.FichaCompleta)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.codigo2)
                .IsUnicode(false);

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.Origen)
                .IsUnicode(false);

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.Paises)
                .IsUnicode(false);

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<ComercialesAgentes>()
                .Property(e => e.Nota)
                .HasPrecision(18, 5);

            modelBuilder.Entity<CursosHistorico>()
                .Property(e => e.Fecha)
                .HasPrecision(0);

            modelBuilder.Entity<CursosHistorico>()
                .Property(e => e.Nota)
                .HasPrecision(18, 5);
        }
    }
}
