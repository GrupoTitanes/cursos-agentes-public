# Project Title
  Cursos Formación Agentes

## Project Description
 This application is designed for made multiplie versions of courses. The content of those courses may be check by SEPBLAC (The Executive Service of the Commission for the Prevention of Money Laundering and Monetary Infractions (Sepblac) is the Financial Intelligence Unit of Spain, being unique throughout the national territory. The Sepblac is also the Supervisory Authority for the prevention of money laundering and the financing of terrorism).
 When the course is approved, the user obtains a certificate that allows him to work in credit entities.

## Authors
 · TITANES COMUNICACIONES S.A.

## License
This project is licensed under The 3-Clause BSD License - see the [LICENSE.md](LICENSE.md) file for details