﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using CursosAgentes.Contextos;
using CursosAgentes.Models;
using CursosAgentes.Resources;
using log4net;

namespace CursosAgentes.Controllers
{
    public class HomeController : BaseController
    {

        ComercialesAgentes usu_com = null;
        AgentesUsuarios ag = null;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ActionResult Index(string pais)
        {
            if (pais != "IT")
            {
                Session["logopais"] = "ES";

            }
            else {

                var language = new CultureInfo("it-IT");
                Thread.CurrentThread.CurrentUICulture = language;
                Session["logopais"] = "IT";
            }

           
            return View();
        }

        public ActionResult listarCursosUsuarios()
        {
            List<CursosCursos_Usuarios> listaCursosUsuarios = ManagerService.contextES.CursosCursos_Usuarios.ToList();

            ViewBag.listaCursosUsuarios = listaCursosUsuarios;

            return View();
        }

        [HttpPost]
        public ActionResult Login()
        {
            try {


                string dni = Request.Form["dni"].ToUpper();
                string telefono = Request.Form["usuario"].ToUpper();
                string password = Request.Form["password"].ToUpper();
                string contexto = string.Empty;

                if (dni.Length < 2 && telefono.Length < 2 && password.Length < 2)
                {

                    ViewBag.ErrorLogin = CursosAgentesResources.ErrorAcess;
                    return View("Index");

                }
                else
                {

                    usu_com = ManagerService.FindByDniPhonePasswordProspecto(dni, telefono, password, out contexto);

                    ag = ManagerService.FindByDniPhonePasswordObject(usu_com);


                    if (!string.IsNullOrEmpty(contexto))
                        Session["contexto"] = contexto;                    


                    if (ag != null)
                    {
                        return RedirectToAction("Index", "Cursos", new { dni = ag.Documento.ToString(), usuario = ag.Login, password = ag.Documento.ToString() });
                    }
                    else
                    {

                        if (usu_com != null)
                        {

                            return RedirectToAction("IndexProspecto", "Cursos", new { idProspecto = usu_com.id_agente });

                        }
                        else
                        {
                            ViewBag.ErrorLogin = CursosAgentesResources.ErrorAcess; 

                            return View("Index");
                        }

                    }

                }
                
               

            }
            catch (Exception e) {
                Log.Info("Error al acceder || " + e);

                return View("Index");
            }

        }

        public ActionResult cerrarSession()
        {
            Session.Abandon();

            return View("Index");
        }
    }
}