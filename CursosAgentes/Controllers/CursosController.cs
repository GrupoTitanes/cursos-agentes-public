﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CursosAgentes.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using log4net;
using CursosAgentes.Contextos;
using CursosAgentes.Resources;
using System.Globalization;
using System.Threading;

namespace CursosAgentes.Controllers
{
    public class CursosController : BaseController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        CursosUsuarios usu = new CursosUsuarios();
        CursosCursos curs = new CursosCursos();
        CursosCursos_Usuarios curs_usu = new CursosCursos_Usuarios();
        CursosDocumentaciones doc = new CursosDocumentaciones();
        CursosTexts text = new CursosTexts();
        CursosPreguntas preg = new CursosPreguntas();
        CursosPuntuaciones_Preguntas punt_resp = new CursosPuntuaciones_Preguntas();
        CursosPuntuaciones punt = new CursosPuntuaciones();
        CursosRespuestas resp = new CursosRespuestas();
        DateTime fechahora = DateTime.Now;
        AgentesUsuarios ag = new AgentesUsuarios();
        ComercialesAgentes usu_com = new ComercialesAgentes();
        CursosHistorico curs_histo = new CursosHistorico();
        string contexto = string.Empty;
        
        static string enlaceAgente;

        public ActionResult Index(string dni, string usuario, string password)
        {

            dni = dni.Trim();
            usuario = usuario.Trim();
            password = password.Trim();

            if (Session["contexto"] == null)
            {

                ag = ManagerService.FindByDniPhonePasswordAgente(dni, usuario, password, out string contexto);

                if (!string.IsNullOrEmpty(contexto))
                    Session["contexto"] = contexto;
            }
            else {

                contexto = Session["contexto"].ToString();
                ag = ManagerService.FindByDniPhonePasswordAgenteDirect(dni, usuario, password, contexto);
                if (ag == null) {
                    Session.Abandon();
                    return RedirectToAction("Index","Home");
                }
            }

            if (string.IsNullOrEmpty(contexto))
            {
                if (Session["contexto"] != null)
                    contexto = Session["contexto"].ToString();
            }



            if (ag != null) {

                curs_usu = ManagerService.GetContext(contexto).CursosCursos_Usuarios.Where(x => x.ID_USUARIOS == ag.id_usuarios).FirstOrDefault();

                if (curs_usu == null)
                {
                    curs_usu = new CursosCursos_Usuarios();
                    curs_usu.ID_USUARIOS = ag.id_usuarios;
                    curs_usu.ID_CURSOS = 1;

                    ManagerService.GetContext(contexto).CursosCursos_Usuarios.Add(curs_usu);
                    ManagerService.GetContext(contexto).SaveChanges();

                    Session["idUsuario"] = ag.id_usuarios;
                    Session["nombreUsuario"] = ag.Login;

                    listarCursos();
                    listarCursosUsuarios();
                    listarAgentesUsuario();
                    listarComercialesAgentes();
                    listarUsuarios();

                    CursosController.enlaceAgente = "https://grupotitanes.es:8555/Cursos/Cursos/Index/" + ag.Documento + "/" + ag.Login + "/" + ag.Documento;

                    return View();
                }
                else
                {
                    ag = ManagerService.GetContext(contexto).AgentesUsuarios.Where(x => x.id_usuarios == curs_usu.ID_USUARIOS).FirstOrDefault();
                    if (ag.CursoRealizado.HasValue) {
                        DateTime fechaCaduca = (DateTime)ag.CursoRealizado;
                        fechaCaduca = fechaCaduca.AddDays(350);
                        if (fechaCaduca < fechahora)
                        {
                            punt = ManagerService.GetContext(contexto).CursosPuntuaciones.Where(X => X.ID_USUARIOS == curs_usu.ID_USUARIOS && X.ID_TEXTS == 1).FirstOrDefault();
                            if (punt != null)
                            {
                                ManagerService.GetContext(contexto).CursosPuntuaciones.Remove(punt);
                            }
                            curs_usu.ESTADO = null;

                            ManagerService.GetContext(contexto).SaveChanges();
                        }
                    }


                    Session["idUsuario"] = ag.id_usuarios;
                    Session["nombreUsuario"] = ag.Login;

                    listarCursos();
                    listarCursosUsuarios();
                    listarAgentesUsuario();
                    listarComercialesAgentes();
                    listarUsuarios();

                    CursosController.enlaceAgente = "https://grupotitanes.es:8555/Cursos/Cursos/Index/" + ag.Documento + "/" + ag.Login + "/" + ag.Documento;


                    return View();
                }
            }
            else {
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult IndexProspecto(int idProspecto) {

            if (Session["contexto"] != null)
                contexto = Session["contexto"].ToString();

            curs_usu = ManagerService.GetContext(contexto).CursosCursos_Usuarios.Where(x => x.ID_PROSPECTO == idProspecto).FirstOrDefault();

            if (curs_usu == null)
            {
                curs_usu = new CursosCursos_Usuarios();
                curs_usu.ID_PROSPECTO = idProspecto;
                curs_usu.ID_CURSOS = 1;

                ManagerService.GetContext(contexto).CursosCursos_Usuarios.Add(curs_usu);
                ManagerService.GetContext(contexto).SaveChanges();

                System.Web.HttpContext.Current.Session["prospecto"] = idProspecto;

                return RedirectToAction("accederText", new { id = 1 });
            }
            else if (curs_usu.ESTADO == 1)
            {
                return Content("<script>alert('"+ CursosAgentesResources.CourseAvailable + "');window.location.href = '/Cursos/Home/Index';</script>");
            }
            else {
                System.Web.HttpContext.Current.Session["prospecto"] = idProspecto;

                return RedirectToAction("accederText",new { id=1});
            }
        }

        [HttpPost]
        public ActionResult Documentacion()
        {

            if (string.IsNullOrEmpty(contexto))
            {
                if (Session["contexto"] != null)
                    contexto = Session["contexto"].ToString();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            if ((int)System.Web.HttpContext.Current.Session.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                listarCursos();
                listarCursosUsuarios();

                string idCurso = Request.Form["idCurso"];
                List<CursosDocumentaciones> listDocs = ManagerService.GetContext(contexto).CursosDocumentaciones.ToList();

                foreach (var list in listDocs)
                {
                    if (list.ID_CURSO == Int32.Parse(idCurso))
                    {
                        ViewBag.urlDoc = list.URL;
                    }
                }


                return View();
            }

        }

        public ActionResult listarCursosUsuarios()
        {
            List<CursosCursos_Usuarios> listaCursosUsuarios = ManagerService.GetContext(contexto).CursosCursos_Usuarios.ToList();
            ViewBag.listaCursosUsuarios = listaCursosUsuarios;

            return View();
        }

        public ActionResult listarAgentesUsuario()
        {
            List<AgentesUsuarios> listaAgentesUsuarios = ManagerService.GetContext(contexto).AgentesUsuarios.ToList();
            ViewBag.listaAgentesUsuarios = listaAgentesUsuarios;

            return View();
        }

        public ActionResult listarComercialesAgentes()
        {
            List<ComercialesAgentes> listaComercialesAgentes = ManagerService.GetContext(contexto).ComercialesAgentes.ToList();
            ViewBag.listaComercialesAgentes = listaComercialesAgentes;

            return View();
        }

        public ActionResult listarDocmentaciones()
        {
            List<CursosDocumentaciones> listaDocumentaciones = ManagerService.GetContext(contexto).CursosDocumentaciones.ToList();
            ViewBag.listaDocumentaciones = listaDocumentaciones;

            return View();
        }
        public ActionResult listarText()
        {
            List<CursosTexts> listaText = ManagerService.GetContext(contexto).CursosTexts.ToList();
            ViewBag.listaText = listaText;

            return View();
        }

        public ActionResult listarCursos()
        {
            List<CursosCursos> listaCursos = ManagerService.GetContext(contexto).CursosCursos.ToList();
            ViewBag.listaCursos = listaCursos;

            return View();
        }

        public ActionResult listarRespuestas()
        {
            List<CursosRespuestas> listaRespuestas = ManagerService.GetContext(contexto).CursosRespuestas.ToList();
            ViewBag.listaRespuestas = listaRespuestas;

            return View();
        }

        public ActionResult listarPreguntas()
        {
            List<CursosPreguntas> listaPreguntas = ManagerService.GetContext(contexto).CursosPreguntas.ToList();
            ViewBag.listaPreguntas = listaPreguntas;

            return View();
        }

        public ActionResult listarPuntuaciones()
        {

            List<CursosPuntuaciones> listaPuntuaciones = ManagerService.GetContext(contexto).CursosPuntuaciones.ToList();
            ViewBag.listaPuntuaciones = listaPuntuaciones;

            return View();
        }

        public ActionResult listarUsuarios()
        {
            List<CursosUsuarios> listaUsuarios = ManagerService.GetContext(contexto).CursosUsuarios.ToList();
            ViewBag.listaUsuarios = listaUsuarios;

            return View();
        }

        [HttpPost]
        public ActionResult crearCurso(HttpPostedFileBase urlProcedimiento)
        {
            string nombre = Request.Form["nombre"];
            string usuCrea = Request.Form["usuCrea"];
            string ruta = Server.MapPath("//192.168.10.222/Cursos/Temario/");


            curs.NOMBRE = nombre;
            curs.FECHA_CREACION = fechahora;
            curs.USU_CREACION = Int32.Parse(usuCrea);

            ManagerService.GetContext(contexto).CursosCursos.Add(curs);
            ManagerService.GetContext(contexto).SaveChanges();

            doc.NOMBRE = Request.Form["nombreTemario"];
            ManagerService.GetContext(contexto).subirDocumentos(ruta, urlProcedimiento);
            doc.URL = "https://www.grupotitanes.es:8555/Cursos/Temario/" + urlProcedimiento.FileName;
            doc.ID_CURSO = curs.ID;

            ManagerService.GetContext(contexto).CursosDocumentaciones.Add(doc);
            ManagerService.GetContext(contexto).SaveChanges();



            return Content("<script>alert('Curso creado correctamente.');window.location.href = '/Cursos/Cursos/Index';</script>");
        }
        [HttpPost]
        public ActionResult modificarCurso()
        {
            string nombre = Request.Form["nombre"];
            string id_curso = Request.Form["id_curso"];

            var curs = ManagerService.GetContext(contexto).CursosCursos.Find(Int32.Parse(id_curso));

            curs.NOMBRE = nombre;

            ManagerService.GetContext(contexto).SaveChanges();

            return Content("<script>alert('Curso modificado correctamente a " + ManagerService.GetContext(contexto).CursosCursos.Find(Int32.Parse(id_curso)).NOMBRE + ".');window.location.href = '/Cursos/Cursos/Index';</script>");
        }

        [HttpPost]
        public ActionResult eliminarCurso()
        {

            string id_curso = Request.Form["id_curso"];
            var curs = ManagerService.GetContext(contexto).CursosCursos.Find(Int32.Parse(id_curso));
            string nombreBorrado = (String)ManagerService.GetContext(contexto).CursosCursos.Find(Int32.Parse(id_curso)).NOMBRE;

            List<CursosCursos_Usuarios> listaEliminar = ManagerService.GetContext(contexto).CursosCursos_Usuarios.ToList();
            List<CursosPuntuaciones> listaPuntuaciones = ManagerService.GetContext(contexto).CursosPuntuaciones.ToList();
            List<CursosPreguntas> listaPreguntas = ManagerService.GetContext(contexto).CursosPreguntas.ToList();
            List<CursosRespuestas> listaRespuestas = ManagerService.GetContext(contexto).CursosRespuestas.ToList();
            List<CursosPuntuaciones_Preguntas> listaPuntPreg = ManagerService.GetContext(contexto).CursosPuntuaciones_Preguntas.ToList();
            List<CursosTexts> listaTexts = ManagerService.GetContext(contexto).CursosTexts.ToList();

            foreach (var list in listaTexts)
            {

                foreach (var list2 in listaPuntuaciones)
                {
                    if (list2.ID_TEXTS == list.ID && list.ID_CURSO == Int32.Parse(id_curso))
                    {
                        foreach (var listPP in listaPuntPreg)
                        {
                            if (listPP.ID_TEXTS == list.ID)
                            {
                                ManagerService.GetContext(contexto).CursosPuntuaciones_Preguntas.Remove(listPP);
                            }
                        }

                        ManagerService.GetContext(contexto).CursosPuntuaciones.Remove(list2);

                        foreach (var listP in listaPreguntas)
                        {
                            if (listP.ID_TEXT == list.ID)
                            {
                                foreach (var listR in listaRespuestas)
                                {
                                    if (listR.ID_PREGUNTA == listP.ID)
                                    {
                                        ManagerService.GetContext(contexto).CursosRespuestas.Remove(listR);
                                    }
                                }

                                ManagerService.GetContext(contexto).CursosPreguntas.Remove(listP);
                            }
                        }


                    }
                }
            }

            foreach (var list in listaEliminar)
            {
                if (list.ID_CURSOS == Int32.Parse(id_curso))
                {
                    ManagerService.GetContext(contexto).CursosCursos_Usuarios.Remove(list);
                }
            }


            ManagerService.GetContext(contexto).CursosCursos.Remove(curs);
            ManagerService.GetContext(contexto).SaveChanges();

            return Content("<script>alert('Curso " + nombreBorrado + " eliminado correctamente.');window.location.href = '/Cursos/Cursos/Index';</script>");
        }


        public ActionResult accederCurso(int id)

        {

            if (string.IsNullOrEmpty(contexto))
            {
                if (Session["contexto"] != null)
                    contexto = Session["contexto"].ToString();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            try
            {
                //COMPRUEBA SI EL USUARIO ES UN PROSPECTO PARA REDIRECCIONARLE AL TEXT

                if (System.Web.HttpContext.Current.Session["prospecto"] != null)
                {
                    listarCursos();
                    listarText();

                    return Content("<script>window.location.href = '/Cursos/Cursos/accederText?id=" + 1 + "';</script>");
                }
                //COMPRUEBA SI EL USUARIO ES UN PROSPECTO PARA REDIRECCIONARLE AL TEXT

                listarCursos();
                listarCursosUsuarios();
                listarText();
                listarUsuarios();
                listarPuntuaciones();

                int id_usu = (int)System.Web.HttpContext.Current.Session["idUsuario"];

                //COMPRUEBA SI YA DISPONE DE UN REGISTRO EN LA TABLA CURSOS_USUARIOS PARA NO DULICARLO
                try {

                    ManagerService.GetContext(contexto).CursosCursos_Usuarios.Where(x => x.ID_USUARIOS == id_usu && x.ID_CURSOS == id).First();

                } catch (Exception) {
                    curs_usu.ID_USUARIOS = id_usu;
                    curs_usu.ID_CURSOS = id;
                    ManagerService.GetContext(contexto).CursosCursos_Usuarios.Add(curs_usu);
                    ManagerService.GetContext(contexto).SaveChanges();

                    return View(descCurso(id));
                }


                //COMPRUEBA SI YA DISPONE DE UN REGISTRO EN LA TABLA CURSOS_USUARIOS PARA NO DULICARLO

                return View(descCurso(id));
            }
            catch (Exception)
            {

                return RedirectToAction("Index", "Home");
            }

        }

        public CursosCursos descCurso(int id)
        {
            curs = ManagerService.GetContext(contexto).CursosCursos.Find(id);
            return curs;
        }

        public ActionResult accederText(int id)

        {
            if (string.IsNullOrEmpty(contexto))
            {
                if (Session["contexto"] != null)
                    contexto = Session["contexto"].ToString();
            }
            else {
                return RedirectToAction("Index", "Home");
            }
            

            try {
                
                listarCursos();
                listarCursosUsuarios();
                listarDocmentaciones();
                listarText();

                List<CursosTexts> listaTexts = ManagerService.GetContext(contexto).CursosTexts.ToList();
                List<CursosPreguntas> listaPreguntasFiltrado = ManagerService.GetContext(contexto).CursosPreguntas.Where(x => x.ID_TEXT == id).ToList();
                List<CursosRespuestas> listaRespuestasFiltrado = ManagerService.GetContext(contexto).CursosRespuestas.ToList();

                ViewBag.listaPreguntasFiltrado = listaPreguntasFiltrado;
                ViewBag.listaRespuestasFiltrado = listaRespuestasFiltrado;

                return View(descText(id));
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Home");
            }

        }


        public CursosTexts descText(int id)
        {
            text = ManagerService.GetContext(contexto).CursosTexts.Where(x => x.ID == id).First();
            listarCursos();
            listarCursosUsuarios();

            return text;
        }

        [HttpPost]
        public ActionResult crearTextCurso()
        {
            var nombreText = Request.Form["nombreText"];
            var id_curso = Int32.Parse(Request.Form["id_curso"]);


            text.NOMBRE = nombreText;
            text.ID_CURSO = id_curso;
            text.FECHA_CREACION = fechahora;

            List<CursosTexts> ultimoText = ManagerService.GetContext(contexto).CursosTexts.Where(x => x.ID_CURSO == id_curso).OrderByDescending(x => x.POSICION).ToList();

            if (ultimoText.Count() != 0)
            {
                text.POSICION = (ultimoText.First().POSICION + 1);

            }
            else
            {
                text.POSICION = 1;
            }

            ManagerService.GetContext(contexto).CursosTexts.Add(text);
            ManagerService.GetContext(contexto).SaveChanges();

            for (int i = 1; i < 11; i++)
            {

                preg.TEXTO = Request.Form["pregunta-" + i];
                preg.ID_TEXT = text.ID;

                ManagerService.GetContext(contexto).CursosPreguntas.Add(preg);

                for (int x = 1; x < 5; x++)
                {

                    resp.TEXTO = Request.Form["respuesta-" + i + "-" + x];
                    resp.ID_PREGUNTA = preg.ID;

                    if (Int32.Parse(Request.Form["respuestaCorrecta-" + i]) == x)
                    {
                        resp.CORRECTA = 1;
                    }
                    else
                    {
                        resp.CORRECTA = 0;
                    }

                    ManagerService.GetContext(contexto).CursosRespuestas.Add(resp);

                    ManagerService.GetContext(contexto).SaveChanges();
                }

                ManagerService.GetContext(contexto).SaveChanges();

                if (i == 10)
                {
                    return Content("<script>alert('Text creado correctamente.');window.location.href = '/Cursos/Cursos/accederCurso?id=" + id_curso + "';</script>");

                }
            }
            return View();

        }


        [HttpPost]
        public ActionResult insertarTextUsuario()
        {
            if (string.IsNullOrEmpty(contexto))
            {
                if (Session["contexto"] != null)
                    contexto = Session["contexto"].ToString();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            int idUsu = 0;
            int idProspecto = 0;
 
            if (System.Web.HttpContext.Current.Session["prospecto"] != null)
            {
                idProspecto = (int)System.Web.HttpContext.Current.Session["prospecto"];

            }
            else {
                try
                {
                    idUsu = (int)System.Web.HttpContext.Current.Session["idUsuario"];
                }
                catch(Exception e) {
                    return Content("<script>alert('" + CursosAgentesResources.TextTimeOut + "');javascript:window.location.href='"+ CursosController.enlaceAgente + "';</script>");
                   
                }

            }

            List<CursosRespuestas> listaRespuestas = ManagerService.GetContext(contexto).CursosRespuestas.ToList();
            int nota = 0;
            int puntuaciones = 0;
            char delimiter = ',';
            string[] todasRespuestas = null;
            int numeroRespuestas = 0;

            for (int i = 1; i < 11; i++)
            {
                int contador = 0;
                int idPreg = Int32.Parse(Request.Form["pregunta-" + i]);
                List<CursosRespuestas> listaRespuestasCorrectas = ManagerService.GetContext(contexto).CursosRespuestas.Where(x => x.ID_PREGUNTA == idPreg && x.CORRECTA == 1).ToList();

                var respuestas = Request.Form["respuesta-usu-" + i];
                if (respuestas != null) {
                    numeroRespuestas++;
                }
                try { todasRespuestas = respuestas.Split(delimiter); } catch { return Content("<script>alert('"+ CursosAgentesResources.TextErrorAnswers + "');javascript:window.history.back();</script>"); }

                if (i==10 && numeroRespuestas != 10) {
                    return Content("<script>alert('"+ CursosAgentesResources.TextErrorAnswers + "');javascript:window.history.back();</script>");
                }

                int numRegistros = ManagerService.GetContext(contexto).CursosRespuestas.Where(x => x.ID_PREGUNTA == idPreg && x.CORRECTA == 1).Count();
                int prueba = respuestas.IndexOf(",");

                foreach (var list in listaRespuestasCorrectas)
                {
                    if (respuestas.IndexOf(",") != -1 && numRegistros > 1)
                    {
                        if (todasRespuestas.Length < 3)
                        {
                            foreach (var resp in todasRespuestas)
                            {
                                if (list.ID == Int32.Parse(resp))
                                {
                                    contador = contador + 1;
                                }
                            }

                            if (contador == 2)
                            {

                                punt_resp.ID_PREG = list.ID;
                                punt_resp.FECHA_CREACION = fechahora;
                                punt_resp.ID_TEXTS = Int32.Parse(Request.Form["text"]);                               
                                punt_resp.VALOR = 5;
                                nota += 10;

                            }
                            else
                            {
                                punt_resp.ID_PREG = list.ID;
                                punt_resp.FECHA_CREACION = fechahora;
                                punt_resp.ID_TEXTS = Int32.Parse(Request.Form["text"]);

                                if (System.Web.HttpContext.Current.Session["prospecto"] != null) {
                                    punt_resp.ID_PROSPECTO = idProspecto;
                                }
                                else {
                                    punt_resp.ID_USUARIOS = idUsu;
                                }

                                punt_resp.VALOR = 0;


                            }

                            ManagerService.GetContext(contexto).CursosPuntuaciones_Preguntas.Add(punt_resp);
                            ManagerService.GetContext(contexto).SaveChanges();
                        }
                        else
                        {
                            foreach (var resp in todasRespuestas)
                            {
                                punt_resp.ID_PREG = list.ID;
                                punt_resp.FECHA_CREACION = fechahora;
                                punt_resp.ID_TEXTS = Int32.Parse(Request.Form["text"]);
                                if (System.Web.HttpContext.Current.Session["prospecto"] != null)
                                {
                                    punt_resp.ID_PROSPECTO = idProspecto;
                                }
                                else
                                {
                                    punt_resp.ID_USUARIOS = idUsu;
                                }
                                punt_resp.VALOR = 0;
                            }
                            ManagerService.GetContext(contexto).CursosPuntuaciones_Preguntas.Add(punt_resp);
                            ManagerService.GetContext(contexto).SaveChanges();
                        }



                    }
                    else if (respuestas.IndexOf(",") == -1 && numRegistros > 1 && todasRespuestas.Length < 3)
                    {
                        punt_resp.ID_PREG = list.ID;
                        punt_resp.FECHA_CREACION = fechahora;
                        punt_resp.ID_TEXTS = Int32.Parse(Request.Form["text"]);
                        if (System.Web.HttpContext.Current.Session["prospecto"] != null)
                        {
                            punt_resp.ID_PROSPECTO = idProspecto;
                        }
                        else
                        {
                            punt_resp.ID_USUARIOS = idUsu;
                        }
                        punt_resp.VALOR = 0;
                        nota += 0;

                        ManagerService.GetContext(contexto).CursosPuntuaciones_Preguntas.Add(punt_resp);
                        ManagerService.GetContext(contexto).SaveChanges();

                    }

                    else
                    {


                        var respCorrect = ManagerService.GetContext(contexto).CursosRespuestas.Find(Int32.Parse(respuestas));

                        punt_resp.ID_PREG = list.ID;
                        punt_resp.FECHA_CREACION = fechahora;
                        punt_resp.ID_TEXTS = Int32.Parse(Request.Form["text"]);
                        if (System.Web.HttpContext.Current.Session["prospecto"] != null)
                        {
                            punt_resp.ID_PROSPECTO = idProspecto;
                        }
                        else
                        {
                            punt_resp.ID_USUARIOS = idUsu;
                        }

                        if (respCorrect.CORRECTA == 1)
                        {
                            punt_resp.VALOR = 10;
                            nota += 10;
                        }
                        else
                        {
                            punt_resp.VALOR = 0;
                            nota += 0;
                        }

                        ManagerService.GetContext(contexto).CursosPuntuaciones_Preguntas.Add(punt_resp);
                        ManagerService.GetContext(contexto).SaveChanges();
                    }

                }
            }


            if (nota >= 70)
            {
                punt.RESULTADO = nota;
                punt.ID_TEXTS = Int32.Parse(Request.Form["text"]);
                if (System.Web.HttpContext.Current.Session["prospecto"] != null)
                {
                    punt.ID_PROSPECTO = idProspecto;
                }
                else
                {
                    punt.ID_USUARIOS = idUsu;
                }
                punt.ESTADO = 1;

                ManagerService.GetContext(contexto).CursosPuntuaciones.Add(punt);
                ManagerService.GetContext(contexto).SaveChanges();
                List<CursosTexts> listaTexts = ManagerService.GetContext(contexto).CursosTexts.ToList();

                var text = ManagerService.GetContext(contexto).CursosTexts.Find(Int32.Parse(Request.Form["text"]));
                List<CursosTexts> textCurso = ManagerService.GetContext(contexto).CursosTexts.Where(x => x.ID_CURSO == text.ID_CURSO).OrderByDescending(x => x.POSICION).ToList();
                text = textCurso.First();
                List<CursosPuntuaciones> listaPuntuaciones = ManagerService.GetContext(contexto).CursosPuntuaciones.ToList();

                foreach (var list in listaPuntuaciones)
                {
                    if (text.ID == list.ID_TEXTS && list.ID_USUARIOS == idUsu && list.ESTADO == 1 && list.ID_PROSPECTO == null || text.ID == list.ID_TEXTS && list.ID_USUARIOS == null && list.ESTADO == 1 && list.ID_PROSPECTO == idProspecto)
                    {


                        foreach (var punt in textCurso)
                        {
                            foreach (var usu_punt in listaPuntuaciones)
                            {
                                if (usu_punt.ID_TEXTS == punt.ID && usu_punt.ID_USUARIOS == idUsu && list.ID_PROSPECTO == null )
                                {
                                    puntuaciones = puntuaciones + (int)usu_punt.RESULTADO;


                                } else if(list.ID_PROSPECTO == idProspecto)
                                {
                                    puntuaciones= (int)usu_punt.RESULTADO;
                                }

                            }
                        }


                        List<CursosCursos_Usuarios> listaCursoUsu;
                        puntuaciones = puntuaciones / textCurso.Count();

                        if (idUsu == 0)
                        {
                            listaCursoUsu = ManagerService.GetContext(contexto).CursosCursos_Usuarios.Where(x => x.ID_CURSOS == text.ID_CURSO && x.ID_PROSPECTO == idProspecto).ToList();
                        }
                        else {
                            listaCursoUsu = ManagerService.GetContext(contexto).CursosCursos_Usuarios.Where(x => x.ID_CURSOS == text.ID_CURSO && x.ID_USUARIOS == idUsu ).ToList();
                        }
                        

                        var cursoUsu = ManagerService.GetContext(contexto).CursosCursos_Usuarios.Find(listaCursoUsu.First().ID);

                        cursoUsu.RESULTADO = puntuaciones/10;
                        cursoUsu.FECHA_REALIZADO = fechahora;
                        cursoUsu.ESTADO = 1;

                        if (System.Web.HttpContext.Current.Session["prospecto"] != null)
                        {
                            string nombreUsu = ManagerService.GetContext(contexto).ComercialesAgentes.Where(x=>x.id_agente == idProspecto).First().Nombre + " " + ManagerService.GetContext(contexto).ComercialesAgentes.Where(x => x.id_agente == idProspecto).First().Apellido1 + " " + ManagerService.GetContext(contexto).ComercialesAgentes.Where(x => x.id_agente == idProspecto).First().Apellido2;
                            string curso = ManagerService.GetContext(contexto).CursosCursos.Find(text.ID_CURSO).NOMBRE;

                            ManagerService.GetContext(contexto).SaveChanges();

                            usu_com = ManagerService.GetContext(contexto).ComercialesAgentes.Where(x => x.id_agente == idProspecto).First();

                            usu_com.Nota = puntuaciones / 10;
                            usu_com.CursoRealizado = fechahora;

                            curs_histo = new CursosHistorico();
                            curs_histo.IdProspecto = idProspecto;
                            curs_histo.Fecha = fechahora;
                            curs_histo.IdCurso = (Int32)cursoUsu.ID_CURSOS;
                            curs_histo.Nota = puntuaciones / 10;

                            ManagerService.GetContext(contexto).CursosHistorico.Add(curs_histo);

                            ManagerService.GetContext(contexto).SaveChanges();
                            generarCertificado(nombreUsu, curso);
                            Session.Abandon();
                            //return Content("<script>alert('"+ CursosAgentesResources.CourseApproved + "');window.location.replace('https://www.grupotitanes.es:8555/Cursos/Certificados/CertificadoCurso.pdf');</script>");
                            return Content("<script>alert('" + CursosAgentesResources.CourseApproved + "');window.location.replace('/Certificados/CertificadoCurso.pdf');</script>");

                        }
                        else
                        {

                            ManagerService.GetContext(contexto).SaveChanges();
                            
                            ag = ManagerService.GetContext(contexto).AgentesUsuarios.Where(x => x.id_usuarios == idUsu).First();
                            List<AgentesUsuarios> mismoUsuario = ManagerService.GetContext(contexto).AgentesUsuarios.Where(x => x.Documento == ag.Documento && x.Validado != "N").ToList();

                            foreach (var usuMismo in mismoUsuario) {
                                usuMismo.Nota = puntuaciones / 10;
                                usuMismo.CursoRealizado = fechahora;
                                curs_histo = new CursosHistorico();


                                curs_histo.IdAgente = usuMismo.id_usuarios;
                                curs_histo.Fecha = fechahora;
                                curs_histo.IdCurso = (Int32)cursoUsu.ID_CURSOS;
                                curs_histo.Nota = puntuaciones / 10;

                                ManagerService.GetContext(contexto).CursosHistorico.Add(curs_histo);
                                //ManagerService.GetContext(contexto).SaveChanges();


                                if (usuMismo.id_usuarios != idUsu) {
                                    curs_histo = new CursosHistorico();
                                    curs_usu.ESTADO = 1;
                                    curs_usu.FECHA_REALIZADO = fechahora;
                                    curs_usu.ID_CURSOS = 1;
                                    curs_usu.ID_USUARIOS = usuMismo.id_usuarios;
                                    curs_usu.RESULTADO = puntuaciones / 10;

                                    ManagerService.GetContext(contexto).CursosCursos_Usuarios.Add(curs_usu);

                                }
                            }


                            ManagerService.GetContext(contexto).SaveChanges();

                            return Content("<script>alert('"+CursosAgentesResources.CourseApproved + "');window.location.replace('/Cursos/Cursos/accederCurso?id=" + text.ID_CURSO + "');</script>");
                        }

                    }
                }

                return Content("<script>alert('"+CursosAgentesResources.TextApproved + " " + nota / 10 + ".');window.location.replace('/Cursos/Cursos/accederCurso?id=" + text.ID_CURSO + "');</script>");



            }
            string rutaText = null;

            if (Session["prospecto"] == null)
                rutaText = "/Cursos/Cursos/accederCurso?id=" + ManagerService.GetContext(contexto).CursosTexts.Find(Int32.Parse(Request.Form["text"])).ID_CURSO;
            else 
                rutaText = "/Cursos/Cursos/accederText?id=" + ManagerService.GetContext(contexto).CursosTexts.Find(Int32.Parse(Request.Form["text"])).ID_CURSO;

            
            return Content("<script>alert('"+ CursosAgentesResources.TextNoApproved + "');window.location.replace('" + rutaText + "');</script>");

        }

        [HttpPost]
        public ActionResult verCertificado()
        {
            if (string.IsNullOrEmpty(contexto))
            {
                if (Session["contexto"] != null)
                    contexto = Session["contexto"].ToString();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            if ((int)System.Web.HttpContext.Current.Session.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                int idCurso = Int32.Parse(Request.Form["idCurso"]);
                var usu = ManagerService.GetContext(contexto).AgentesUsuarios.ToList();
                foreach (var u in usu)
                {
                    if (u.id_usuarios == (int)System.Web.HttpContext.Current.Session["idUsuario"])
                    {
                        ag = ManagerService.GetContext(contexto).AgentesUsuarios.Find(u.id_usuarios);
                        
                    }
                }
                string nombreCurso = ManagerService.GetContext(contexto).CursosCursos.Find(idCurso).NOMBRE;
                generarCertificado(ag.Nombre + " " + ag.Apellidos+ " - "+ag.Agente , nombreCurso);
                Session.Abandon();

                //return Content("<script>window.location.href = '/Cursos/Certificados/CertificadoCurso.pdf';</script>");
                return Content("<script>window.location.href = '/Certificados/CertificadoCurso.pdf';</script>");
            }

        }


        public string generarCertificado(string nombre, string curso)
        {

            //string direccion1 = "//192.168.10.222/Cursos/Certificados/CertificadoCurso.pdf";
            string direccion1 = "C:/Repositorios/Cursos Agentes/CursosAgentes/Certificados/CertificadoCurso.pdf";
            //string direccion2 = "https://www.grupotitanes.es:8555/Cursos/Certificados/CertificadoCurso.pdf";
            string direccion2 = "localhost:53549/Certificados/CertificadoCurso.pdf";
            Document doc = new Document(PageSize.A4.Rotate(), 50, 50, 100, 0);
            PdfWriter.GetInstance(doc, new FileStream(direccion1, FileMode.Create));
            doc.Open();

            //Chunk espacio = new Chunk(" ");
            Paragraph text = new Paragraph();
            Paragraph espacio = new Paragraph(" ");
            Paragraph text1 = new Paragraph();
            Paragraph text2 = new Paragraph();
            Paragraph text3 = new Paragraph();
            Paragraph text4 = new Paragraph();
            Paragraph text5 = new Paragraph();

            text.Add(espacio);
            text.Add(espacio);
            text.Add(espacio);

            doc.Add(text);

            text1.Font = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 25f, new BaseColor(255, 140, 0));
            text1.Alignment = Element.ALIGN_CENTER;
            text1.Add(CursosAgentesResources.CertificateImprovement);

            text1.Add(espacio);
            text1.Add(espacio);

            doc.Add(text1);

            text2.Alignment = Element.ALIGN_CENTER;
            text2.Font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 15f);
            text2.Add(CursosAgentesResources.CertificateRegister);
            text2.Add(espacio);
            text2.Add(espacio);

            doc.Add(text2);

            text3.Alignment = Element.ALIGN_CENTER;
            text3.Font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 20f);
            text3.Add(nombre);
            text3.Add(espacio);
            text3.Add(espacio);

            doc.Add(text3);

            text4.Alignment = Element.ALIGN_CENTER;
            text4.Font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 15f);
            text4.Add(CursosAgentesResources.CertificateApproved);
            text4.Add(espacio);
            text4.Add(espacio);

            doc.Add(text4);

            text5.Font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 17f);
            text5.Alignment = Element.ALIGN_CENTER;
            text5.Add(curso + " - " + fechahora.Year);
            text5.Add(espacio);
            text5.Add(espacio);

            doc.Add(text5);

            iTextSharp.text.Image img = Image.GetInstance("//192.168.10.222/Cursos/img/logomoneytrasnfer.png");
            img.Alignment = Element.ALIGN_RIGHT;
            img.ScaleAbsolute(159f, 125f);

            doc.Add(img);
            doc.Close();

            return direccion2;
        }

    }
}