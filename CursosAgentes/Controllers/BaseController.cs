﻿using CursosAgentes.Contextos;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CursosAgentes.Controllers
{
    public class BaseController : Controller
    {
        protected ManagerService ManagerService;
        
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (ManagerService == null)
                ManagerService = new ManagerService();
        }
    }
}