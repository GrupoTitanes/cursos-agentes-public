
/****** Object:  Table [dbo].[CursosHistorico]    Script Date: 29/04/2019 16:28:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CursosHistorico](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCurso] [int] NOT NULL,
	[Fecha] [datetime2](0) NOT NULL,
	[Nota] [decimal](18, 5) NOT NULL,
	[IdAgente] [int] NULL,
	[IdProspecto] [int] NULL,
 CONSTRAINT [PK_CursosHistorico] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CursosHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CursosHistorico_AgentesUsuarios1] FOREIGN KEY([IdAgente])
REFERENCES [dbo].[AgentesUsuarios] ([id_usuarios])
GO

ALTER TABLE [dbo].[CursosHistorico] CHECK CONSTRAINT [FK_CursosHistorico_AgentesUsuarios1]
GO

ALTER TABLE [dbo].[CursosHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CursosHistorico_ComercialesAgentes] FOREIGN KEY([IdProspecto])
REFERENCES [dbo].[ComercialesAgentes] ([id_agente])
GO

ALTER TABLE [dbo].[CursosHistorico] CHECK CONSTRAINT [FK_CursosHistorico_ComercialesAgentes]
GO


