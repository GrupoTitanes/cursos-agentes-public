SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CursosPreguntas](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TEXTO] [varchar](max) NULL,
	[ID_TEXT] [int] NULL,
 CONSTRAINT [PK_PREGUNTAS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CursosPreguntas] ON 

INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (1, N'Uno degli obblighi più importanti in materia di prevenzione al riciclaggio di capitali e finaziamento al terrorismo è:
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (2, N'L’inosservanza delle obbligazioni che, come dipendente, agente o dipendente di un agente mi corrispondono:
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (3, N'Quali di questi documenti sono ammessi dalla normativa vigente come documenti validi per identificare un cliente? (può esserci più di una risposta corretta):
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (4, N'Le entità hanno l’obbligo di realizzare una formazione in materia di prevenzione al riciclaggio di capitali e di finanziamento al terorismo a:
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (5, N'Quali delle seguenti azioni possono essere considerate indizi di riciclaggio di capitali e di finanziamento al terorismo? (può esserci più di una risposta corretta):
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (6, N'Indicare le tappe generiche del Riciclaggio di Capitali:
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (7, N'Qual’è il tempo legale minimo per la conservazione della documentazione?
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (8, N'Un’operazione sospetta è  (può esserci più di una risposta corretta):
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (9, N'L’inosservanza dell’obbligo di comunicare le operazioni sospette:
', 1)
INSERT [dbo].[CursosPreguntas] ([ID], [TEXTO], [ID_TEXT]) VALUES (10, N'Riciclaggio di Capitali  (può esserci più di una risposta corretta):
', 1)
SET IDENTITY_INSERT [dbo].[CursosPreguntas] OFF
