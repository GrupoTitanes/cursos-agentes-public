SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CursosRespuestas](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TEXTO] [varchar](max) NULL,
	[ID_PREGUNTA] [int] NULL,
	[CORRECTA] [int] NULL,
 CONSTRAINT [PK_RESPUESTAS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CursosRespuestas] ON 

INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (1, N'Identificare solo i clienti abituali', 1, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (2, N'Identificazione e conoscenza di tutti i clienti', 1, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (3, N'La conservazione dei documenti dei clienti abituali', 1, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (4, N'Nessuna delle risposte precedenti è corretta', 1, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (5, N'Saranno soggette a sanzioni disciplinari da parte della mia azienda', 2, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (6, N'Possono essere considerate reato e per tanto punite con la reclusione se si partecipa o collabora a un reato di riciclaggio di capitali o di finanziamento al terrorismo', 2, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (7, N'Le due risposte precedenti sono corrette', 2, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (8, N'Nessuna delle risposte precedenti è corretta', 2, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (9, N'Originale della Patente di guida', 3, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (10, N'Originale del documento d’identità per le persone di nazionalità Spagnola sempre che sia in vigore', 3, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (11, N'Originale del permesso di soggiorno sempre che sia in vigore', 3, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (12, N'Nessuna delle risposte precedenti è corretta', 3, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (13, N'I dirigenti dell’entità che sono persone che permettono il funzionamento corretto dell’azienda', 4, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (14, N'Tutti i dipendenti e gli agenti dell’entità', 4, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (15, N'Non ha l’obbligo di realizzare il corso di formazione', 4, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (16, N'Nessuna delle risposte precedenti è corretta', 4, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (17, N'Clienti che potrebbero essere diretti da una terza persona', 5, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (18, N'Clienti che realizzano ripetutamente invii di un importo prossimo ai 999 euro con lo scopo di nascondere e giustificare l’origine del denaro', 5, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (19, N'Clienti che presentano tutti i documenti necessari per realizzare l’operazione', 5, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (20, N'Un cliente abituale che realizza invii di importo sempre simile e sempre agli stessi beneficiari', 5, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (21, N'Trasferimento, conversione, integrazione', 6, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (22, N'Posizionamento e occultamento', 6, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (23, N'Posizionamento, occultamento, integrazione', 6, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (24, N'Nessuna delle risposte precedenti è corretta', 6, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (25, N'5 anni', 7, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (26, N'6 anni', 7, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (27, N'9 anni', 7, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (28, N'10 anni', 7, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (29, N'Qualunque operazione, indipendentemente dall’importo, può essere apparentemente vincolata con il riciclaggio di capitali o al finanziamento al terrorismo ', 8, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (30, N'Qualunque operazione, che supera la quantità di 1.500 euro, può essere apertamente vincolata al riciclaggio di capitali o al finanziamento al terrorismo.', 8, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (31, N'Qualunque operazione insolita, che non ha un fine economico lecito o che presenti indizi di frode', 8, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (32, N'Nessuna delle risposte precedenti è corretta', 8, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (33, N'Si considera un’infrazione grave alla normativa vigente, e per tanto, si può arrivare ad importanti sanzioni economiche', 9, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (34, N'Si considera un’infrazione molto grave alla normativa vigente, e per tanto, si può arrivare alla chiusura del negozio', 9, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (35, N'Le due risposte sono corrette', 9, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (36, N'Le due risposte sono scorrette', 9, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (37, N'È un reato che consiste nel nascondere l’origine di beni o capitali ottenuti in modo illegale in modo che sembri che questi beni o capitali provengano da una fonte legale', 10, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (38, N'È un insieme di meccanismi o procedimenti con i quali si vuole dare un’apparenza di leggitimità o legalità a beni o attivi provenienti da attività illecite', 10, 1)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (39, N'È un reato che consiste nel realizzare transazioni e movimenti di capitali per contribuire alle finalità perseguite da gruppi de organizzazioni terroristiche', 10, 0)
INSERT [dbo].[CursosRespuestas] ([ID], [TEXTO], [ID_PREGUNTA], [CORRECTA]) VALUES (40, N'Nessuna delle risposte precedenti è corretta', 10, 0)
SET IDENTITY_INSERT [dbo].[CursosRespuestas] OFF
